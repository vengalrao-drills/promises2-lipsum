// /*
//     Problem 1:

//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Create a directory of random JSON files
//         2. Delete those files simultaneously 
// */

const fs = require("fs").promises;
const path = require("path");


const makeAndDelete = () => {
    return new Promise(async (resolve, reject) => {

        try {
            let filesCreated = Math.floor(Math.random() * 10) + 1;
            let deleteCreated = filesCreated

            let newDirectory = "RandomFolderooo";
            let filepath = path.join(__dirname, newDirectory)
            fs.mkdir(filepath, { recursive: true }).then(() => {
                let createFile = async (index) => {
                    if (index <= filesCreated) {
                        let fileName = `RandomFile${index}.json`;
                        let filepath = path.join(__dirname, newDirectory, fileName);
                        let content = `this file name is RandomFile${index}`; // for every path filepath is different. recursively it is created

                        fs.writeFile(filepath, content, 'utf-8').then(() => {
                            console.log("file created -,",)
                            index += 1
                            createFile(index)

                        }).catch((err) => {
                            console.log(`error in writing the filepath  - -- - ${err}`);
                        })

                    } else {

                        const delay = async (num) => {
                            console.log(`files will be deleted in ${num} seconds`)
                            return new Promise((resolve, reject) => {
                                setTimeout(() => {
                                    resolve('Deleting files started')
                                }, num * 1000)
                            })

                        }
                        let g = await delay(3)
                        console.log(g)


                        try {
                            console.log("1 - - - - files getting deleting now");
                            let deleteFile = async (delIndex) => {
                                if (delIndex <= deleteCreated) {
                                    let filepath = path.join(__dirname, `/${newDirectory}/RandomFile${delIndex}.json`);
                                    console.log(filepath);
                                    await fs.unlink(filepath).then(() => {
                                        console.log('unlinked ')
                                        deleteFile(delIndex + 1)
                                    }).catch((error) => {
                                        console.log(error)
                                    })
                                } else {
                                    const delay = async (num) => {
                                        console.log(`Directory is going to delete in ${num} seconds`)
                                        return new Promise((resolve, reject) => {
                                            setTimeout(() => {
                                                resolve('')
                                            }, num * 1000)
                                        })
                                    }

                                    let g = await delay(3)
                                    let filepath = path.join(__dirname, `${newDirectory}`);
                                    console.log(filepath);
                                    await fs.rmdir(filepath).then(() => {
                                        resolve("Successfull Created FILES and DELETED")
                                    }).catch((err) => {
                                        reject(`Files, deleted. But error in removing the directory ${err}`)
                                    })
                                }
                            }
                            deleteFile(1)

                        } catch (err) {
                            console.log(err)
                        }
                    }
                };
                createFile(1);


            }).catch((err) => {
                reject(`Unfortunately, Error happened in making a new Directory: ${err}`);
            })
        }
        catch (err) {
            // console.log(err)
            reject(err)
        }
    })
}

module.exports = makeAndDelete;

// makeAndDelete()

