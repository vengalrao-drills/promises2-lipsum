const path = require("path");
const fs = require("fs").promises;

const doOperations = () => {
    return new Promise((resolve, reject) => {
        pathfile = path.join(__dirname, "lipsum1.txt");
        // console.log(pathfile)
        fs.readFile(pathfile, "utf-8").then((data) => {
            // question 2
            // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
            const newFileUpperCase = (data) => {
                // console.log(data)
                const newFilePath = path.join(__dirname, "filesnames2.txt");
                content = data.toUpperCase(); // converting to uppercase
                fs.writeFile(newFilePath, content, "utf-8")
                    .then((data) => {
                        console.log(`Converted the content to uppercase & written to a new file filesnames2.txt\n`);

                        // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
                        const newFileLowerCase = () => {

                            // pathfile = path.join(__dirname, "lipsum1.txt");
                            const newFilePath = path.join(__dirname, "filesnames2.txt");
                            fs.readFile(newFilePath, "utf-8").then((data) => {
                                content = data.toLowerCase(); // converting to LowerCase
                                const newFilePath = path.join(__dirname, "filesnames3.txt");
                                fs.writeFile(newFilePath, content, "utf-8").then(() => {
                                    console.log("Converted the content to lowercase & written to a new file filesnames3.txt\n");


                                    // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                                    const readNewFile = (newFilePath) => {
                                        fs.readFile(newFilePath, "utf-8").then((data) => {
                                            let words = data.split("\n");
                                            words = words.map((word) => {
                                                // will iterate and sort() it
                                                word = word.split(" ");
                                                word = word.sort().join(" ");
                                                return word;
                                            });
                                            words = words.join("\n");
                                            // console.log(words)

                                            const newFilePath4 = path.join(__dirname, "filesnames4.txt");
                                            fs.writeFile(newFilePath4, words, "utf-8").then(() => {
                                                console.log("Read the new files, sort the content & written to a new file filesnames4.txt\n");

                                                // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                                const deleteFiles = async () => {
                                                    const delay = async (num) => {
                                                        console.log(`All files will be deleted in ${num} seconds`)
                                                        return new Promise((resolve, reject) => {
                                                            setTimeout(() => {
                                                                resolve('\nDeleting files started\n')
                                                            }, num * 1000)
                                                        })
                                                    }
                                                    let timer = await delay(5)
                                                    console.log(timer)

                                                    let allFiles = ['filesnames2.txt', 'filesnames3.txt', 'filesnames4.txt']
                                                    allFiles.forEach((element) => {
                                                        let fileName = path.join(__dirname, element)
                                                        fs.unlink(fileName).then(() => {
                                                            resolve("resolved")
                                                        }).catch((err) => {
                                                            reject(`function(deleteFiles) - Unfortunatley, error occured ${err}`)
                                                        })
                                                    })
                                                }
                                                deleteFiles()

                                            }).catch((err) => {
                                                reject(`readNewFile() error occured in creating file ${err}`)
                                            })

                                        }).catch((error) => {
                                            reject(`readNewFile() error occured in creating file ${error}`)
                                        })
                                    }
                                    readNewFile(newFilePath)

                                }).catch((error) => {
                                    reject(`newFileLowerCase() error occured in creating file - -  ${err} `)
                                })

                            }).catch((error) => {
                                reject(`newFileLowerCase() error occured in creating file , ${error}`)
                            })
                        }
                        newFileLowerCase()
                    })
                    .catch((error) => {
                        reject(`newFileUpperCase() error occured in creating file ${error}`)
                    })
            }
            newFileUpperCase(data)

        }).catch((err) => {
            reject(`function (readWordFiles) Error occured in creating the file  ${err}`)
        })
    })
};

module.exports = doOperations;

// doOperations((err, data)=>{
//   if(err){
//     console.log(err)
//   }else{
//     console.log(data)
//   }
// })





